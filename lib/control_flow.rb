# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.split("").each do |x|
    str.delete!(x) if ("a".."z").include?(x)
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid = str.length / 2
  return str[mid] if str.length.odd?
  return str[(mid - 1)..mid] if str.length.even?
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.downcase.count("aeiou")
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = ""
  arr.each_with_index do |x, idx|
    str += x
    str += separator unless arr[idx] == arr.last
  end
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.split("").each_with_index do |x, idx|
    if idx.odd?
      str[idx] = x.upcase
    else
      str[idx] = x.downcase
    end
  end
  str

end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str = str.split(" ").each do |x|
    if x.length >= 5
      x.reverse!
    end
  end
  str.join(" ")

end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  array = []
  (1..n).each do |x|
    if (x % 3 == 0) && (x % 5 ==0)
      array <<  "fizzbuzz"
    elsif x % 3 == 0
      array << "fizz"
    elsif x % 5 == 0
      array << "buzz"
    else
      array << x
    end
  end
  array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse!
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2...num).each do |x|
    return false if num % x == 0
  end
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  array = []
  (1..num).each do |x|
    array << x if num % x == 0
  end
  array
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  array = []
  (2..num).each do |x|
    array << x if (num % x == 0) && prime?(x)
  end
  array
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = []
  even = []
  arr.each do |x|
    odd << x if x.odd?
    even << x if x.even?
  end
  return even[0] if odd.length > even.length
  return odd[0] if even.length > odd.length
end
